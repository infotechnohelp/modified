<?php

namespace Infotechnohelp\Modified\Test\TestCase;

use Infotechnohelp\Modified\Modified;
use PHPUnit\Framework\TestCase;

/**
 * Class ModifiedTest
 * @package PackageSkeleton\Test\TestCase
 */
class ModifiedTest extends TestCase
{
    /** @dataProvider singularPluralData */
    public function testSingular($singular, $plural)
    {
        $this->assertEquals($singular, (new Modified($plural))->singular());
    }

    /** @dataProvider singularPluralData */
    public function testPlural($singular, $plural)
    {
        $this->assertEquals($plural, (new Modified($singular))->plural());
    }


    public function singularPluralData()
    {
        return [
            ['User', 'Users',],
            ['UserMailbox', 'UserMailboxes',],
            ['Index', 'Indices',],
        ];
    }

    /** @dataProvider camelCasedUnderscoredData */
    public function testUnderscored($camelCased, $underscored)
    {
        $this->assertEquals($underscored, (new Modified($camelCased))->underscored());
    }

    /** @dataProvider camelCasedUnderscoredData */
    public function testCamelCased($camelCased, $underscored)
    {
        $this->assertEquals($camelCased, (new Modified($underscored))->camelCased());
    }


    public function camelCasedUnderscoredData()
    {
        return [
            ['Users', 'users',],
            ['UserMailboxes', 'user_mailboxes',],
        ];
    }
}
