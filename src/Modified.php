<?php

namespace Infotechnohelp\Modified;

use Infotechnohelp\Modified\Modifiers\Inflect;

/**
 * Class Modified
 * @package Infotechnohelp\Modified
 */
class Modified
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return $this->getValue();
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function singular(): Modified
    {
        $this->setValue(Inflect::singularize($this->getValue()));

        return $this;
    }

    public function plural(): Modified
    {
        $this->setValue(Inflect::pluralize($this->getValue()));

        return $this;
    }

    public function underscored()
    {
        $this->setValue(strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $this->getValue())));

        return $this;
    }

    public function camelCased()
    {
        $this->setValue(str_replace('_', '', ucwords($this->getValue(), '_')));

        return $this;
    }
}
